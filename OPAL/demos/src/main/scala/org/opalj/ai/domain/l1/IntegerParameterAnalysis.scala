/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj
package ai
package domain
package l1

import scala.language.existentials
import java.net.URL
import scala.Console.BLUE
import scala.Console.BOLD
import scala.Console.GREEN
import scala.Console.RESET
import scala.Iterable
import scala.collection.{ Set, Map }
import scala.collection.parallel.mutable.ParArray
import scala.language.postfixOps
import org.opalj.collection.mutable.Locals
import org.opalj.ai.CorrelationalDomain
import org.opalj.ai.Domain
import org.opalj.ai.InterruptableAI
import org.opalj.ai.IsAReferenceValue
import org.opalj.ai.domain
import org.opalj.ai.ValuesDomain
import org.opalj.br.ClassFile
import org.opalj.br.FieldType
import org.opalj.br.Method
import org.opalj.br.analyses.AnalysisExecutor
import org.opalj.br.analyses.BasicReport
import org.opalj.br.analyses.OneStepAnalysis
import org.opalj.br.analyses.Project
import org.opalj.br.IntLikeType
import org.opalj.br.IntegerType
import org.opalj.collection.immutable.UIDSet
import org.opalj.collection.immutable.UIDSet1
import org.opalj.util.PerformanceEvaluation.time
import org.opalj.ai.common.XHTML

/**
 *  An analysis that tries to determine a refined return ranger of methods with integer parameters and an integer return value.
 *  Moreover the analysis tries to derive a correlation between set parameters and thrown exceptions.
 *
 * @author Marius Diebel
 */
object IntegerParameterAnalysis extends AnalysisExecutor {
    // list yielding (the calling domain, (calling parameter, calling parameter2), expected Results, map of thrown exceptions)
    type CollectedInformation = List[(AnalysisDomain, (AnalysisDomain#IntRange, AnalysisDomain#IntRange), AnalysisDomain#ReturnedValue, Map[PC, AnalysisDomain#Exceptions], AnalysisDomain#Locals)]

    class AnalysisDomain(
        override val project: Project[java.net.URL],
        val ai: InterruptableAI[_],
        val method: Method)
            extends CorrelationalDomain
            with TheProject
            with TheMethod
            with DefaultDomainValueBinding
            with ThrowAllPotentialExceptionsConfiguration
            with DefaultHandlingOfMethodResults
            with IgnoreSynchronization
            with domain.l0.DefaultTypeLevelFloatValues
            with domain.l0.DefaultTypeLevelDoubleValues
            with domain.l0.TypeLevelFieldAccessInstructions
            with domain.l0.TypeLevelInvokeInstructions
            with SpecialMethodsHandling
            with domain.l1.DefaultReferenceValuesBinding
            with domain.l1.DefaultClassValuesBinding
            with domain.l1.DefaultIntegerRangeValues
            with domain.l1.MaxArrayLengthRefinement
            with domain.l1.ConstraintsBetweenIntegerValues
            with domain.l1.DefaultLongValues
            with domain.l1.LongValuesShiftOperators
            with domain.l1.ConcretePrimitiveValuesConversions
            with domain.l2.PerformInvocations
            with domain.l1.LibraryMethodsHandling
            with domain.ProjectBasedClassHierarchy
            with domain.RecordReturnedValuesInfrastructure
            with domain.RecordAllThrownExceptions
            with tools.RecordAnalysisResults
            with TheMemoryLayout {

        type ReturnedValue = List[DomainValue]
        type Exceptions = ThrownException
        type IntRange = IntegerRange

        //type CalledMethodDomain = org.opalj.ai.domain.l2.SharedDefaultDomain[_] with DefaultRecordMethodCallResults
        type CalledMethodDomain = SecondaryInvocationDomain with DefaultRecordMethodCallResults

        private[this] var theReturnedValue: List[DomainValue] = null
        def returnedValue: Option[List[DomainValue]] = Option(theReturnedValue)
        protected[this] def doRecordReturnedValue(pc: PC, value: DomainValue): Unit = {
            if (value.isInstanceOf[IntegerRange]) {
                if (theReturnedValue == null)
                    theReturnedValue = List(value)
                else
                    theReturnedValue = theReturnedValue :+ value
            }
        }

        // Determines the name of the summary file that is being created.
        //def getSummaryName: String = "JDKTestSummary"
        def getSummaryName: String = "null"

        def parameterIndependentAnalysis: Boolean = false

        def isRecursive(classFile: ClassFile, method: Method, operands: Operands): Boolean =
            false

        def shouldInvocationBePerformed(classFile: ClassFile, method: Method): Boolean =
            // method.body.isDefined && filterMethod(method)
            method.body.isDefined && filterMethod(method)

        def calledMethodAI = new InterruptableAI[SecondaryInvocationDomain with DefaultRecordMethodCallResults]

        def calledMethodDomain(classFile: ClassFile, method: Method) =
            new SecondaryInvocationDomain(
                project,
                ai,
                method) with DefaultRecordMethodCallResults

        // some relevant constants
        val positiveVal = IntegerRange(23, 42)
        val negativeVal = IntegerRange(-42, -23)
        val negative = IntegerRange(Int.MinValue, -1)
        val positive = IntegerRange(0, Int.MaxValue)
        val intMin = IntegerRange(Int.MinValue, Int.MinValue)
        val intMax = IntegerRange(Int.MaxValue, Int.MaxValue)
        val completeIntegerRange = IntegerRange(Int.MinValue, Int.MaxValue)
        val zero = IntegerRange(0, 0)
        val plusOne = IntegerRange(1, 1)
        val minusOne = IntegerRange(-1, -1)
        val numberOfCalls = 12
    }

    /*
     * Method to limit the number of analyzed methods in respect to a desired filter.
     * Usually methods with at least an integer parameter and an integer return value should be targeteted.
     * Also it is recommended to only focus on methods that are not private and not package private while creating summaries.
     */
    def filterMethod(method: Method): Boolean = {
        val i = 2
        i match {
            //Different filter settings for the analysis
            case 1  ⇒ (method.returnType.isIntegerType && method.returnType.isBaseType)
            case 2  ⇒ (method.descriptor.parameterTypes.forall { _.isIntegerType } && method.returnType.isIntegerType) && method.descriptor.parameterTypes.filter(_.isIntegerType).size >= 1 && !method.isPrivate && !method.isPackagePrivate
            // 0 to 2 parameter
            case 10 ⇒ (method.descriptor.parameterTypes.exists { _.isIntegerType } && method.descriptor.parametersCount > 0 && method.descriptor.parametersCount <= 2)
            // default: everything integer related
            case _  ⇒ (method.descriptor.parameterTypes.exists { _.isIntegerType } || method.returnType.isIntegerType) && !method.isPrivate && !method.isPackagePrivate
        }
    }

    val analysis = new OneStepAnalysis[URL, BasicReport] {

        override def title: String =
            "Analyse Integer-Parameter"

        override def description: String =
            "Refines integer return ranges and thrown exception in correlation to a set of predefined parameters."

        override def doAnalyze(
            theProject: Project[URL],
            parameters: Seq[String],
            isInterrupted: () ⇒ Boolean) = {

            /*
             * Helper method to initiate writing out the registered results into a summary
             */
            def writeOut(domain: Option[AnalysisDomain]): Unit = {
                if (domain.isDefined) {
                    domain.get.writeOutResults()
                }
            }

            var recordingDomain: Option[AnalysisDomain] = None
            val integerParameterValues = time {
                for {
                    classFile ← theProject.allProjectClassFiles.par
                    method ← classFile.methods
                    if method.body.isDefined
                    if filterMethod(method)
                    parameters = method.parameterTypes
                    ai = new InterruptableAI[Domain]
                    domain = new AnalysisDomain(theProject, ai, method)
                    result = ai(classFile, method, domain)
                } yield {
                    recordingDomain = Option(domain)
                    var collectedInformation: CollectedInformation = List.empty
                    val numberOfIntegerParameters = method.descriptor.parameterTypes.filter(_.isIntegerType).size
                    //println("METHOD: "+classFile.thisType.toJava+"{ "+method.toJava+" }")

                    if (numberOfIntegerParameters == 0) {
                        val locals = ai.initialLocals(classFile, method, domain)(None)
                        collectedInformation = collectInformation(domain, collectedInformation)(None, locals)
                    } else {
                        // iterate over the possible permutations of the parameter configuration
                        // in case we have an even number of int parameters we can skip roughly half of the calls due to the mirrored parameters
                        // unless we want to have a parameter independent mapping
                        for (
                            i ← 1 to domain.numberOfCalls if (i % 2 == 1 || i > 8 || numberOfIntegerParameters % 2 == 1 || domain.parameterIndependentAnalysis)
                        ) {
                            val domain = new AnalysisDomain(theProject, ai, method)
                            val initialLocals = ai.initialLocals(classFile, method, domain)(None)
                            val (intParameter1, intParameter2) = getCallingParameters(i, domain)
                            if (!domain.parameterIndependentAnalysis) {
                                // get a list of possible permutations for the current parameters, 
                                // note that we drop the last parameter in case the number of parameters is uneven
                                val listOfParameterPermutations: Iterator[List[domain.IntegerRange]] =
                                    if (numberOfIntegerParameters == 1)
                                        Iterator(List(intParameter1))
                                    else if (intParameter1 == intParameter2)
                                        Iterator(List.fill(numberOfIntegerParameters)(intParameter1))
                                    else
                                        List.fill((numberOfIntegerParameters + 1) / 2)(List(intParameter1, intParameter2)).flatten.drop(numberOfIntegerParameters % 2).permutations
                                // perform the interpretation for each possible permutation
                                for (l ← listOfParameterPermutations) {
                                    val locals = mapLocals(domain, l.asInstanceOf[List[AnalysisDomain#IntRange]])(initialLocals)
                                    try {
                                        ai.performInterpretation(method.isStrict, method.body.get, domain)(
                                            ai.initialOperands(classFile, method, domain), locals)
                                    } catch {
                                        case t: Throwable ⇒
                                            val state = XHTML.dump(Some(classFile), Some(method), method.body.get, None, domain)(domain.operandsArray, domain.localsArray)
                                            org.opalj.io.writeAndOpen(state, "crash", ".html")
                                            throw t
                                    }
                                    // collect all the necessary information
                                    collectedInformation = collectInformation(domain, collectedInformation)(Some((intParameter1, intParameter2)), locals)
                                }
                            } else {
                                val numberOfIntegerLocals = initialLocals.toSeq.filter { _.isInstanceOf[domain.AnIntegerValue] }.size
                                for {
                                    j ← 0 to numberOfIntegerLocals
                                } {
                                    val locals = createParameterIndependentMapping(domain, intParameter1, j)(initialLocals)
                                    try {
                                        ai.performInterpretation(method.isStrict, method.body.get, domain)(
                                            ai.initialOperands(classFile, method, domain), locals)
                                    } catch {
                                        case t: Throwable ⇒
                                            val state = XHTML.dump(Some(classFile), Some(method), method.body.get, None, domain)(domain.operandsArray, domain.localsArray)
                                            org.opalj.io.writeAndOpen(state, "crash", ".html")
                                            throw t
                                    }
                                    // collect all the necessary information
                                    collectedInformation = collectInformation(domain, collectedInformation)(Some((intParameter1, intParameter2)), locals)
                                }
                            }
                        }
                    }

                    // if any return values or exceptions are caught, create a report and register the results for the summary, otherwise skip
                    if (collectedInformation.exists(_._3.exists(_.isInstanceOf[domain.IntegerRange]))
                        || collectedInformation.exists(_._4.nonEmpty)) {

                        val (str, returnRange, intcount) = createReport(classFile, method, domain)(collectedInformation)
                        val exceptionMap = getMappedOperandsToExceptions(domain)(collectedInformation)
                        domain.registerMethod(
                            method,
                            returnRange,
                            exceptionMap,
                            classFile.thisType)
                        IntergerRefinedValue(str, intcount)
                    }
                }
            } { t ⇒ println("Analysis time "+t.toSeconds) }

            writeOut(recordingDomain)

            // filter out empty entries without relevant/interesting information
            val filteredResults: scala.collection.parallel.mutable.ParArray[IntergerRefinedValue] =
                (integerParameterValues.filter { x ⇒
                    x.isInstanceOf[IntergerRefinedValue]
                }).asInstanceOf[scala.collection.parallel.mutable.ParArray[IntergerRefinedValue]]
            println("Number of more refined-values:  "+filteredResults.map { x ⇒ x.count }.sum)
            BasicReport(
                filteredResults.mkString(
                    "Methods with refined integer return ranges and exceptions ("+
                        filteredResults.size+"): \n", "\n", "\n"))
        }

        /*
         * Helper method to add all the relevant information during the analysis runtime to the recording list.
         */
        private def collectInformation(domain: AnalysisDomain, collection: CollectedInformation)(parameter: Option[(domain.IntegerRange, domain.IntegerRange)], locals: domain.Locals): CollectedInformation = {
            collection :+ ((domain,
                parameter.getOrElse((null, null)),
                domain.returnedValue.getOrElse(List.empty.asInstanceOf[AnalysisDomain#ReturnedValue]),
                domain.allThrownExceptions,
                locals))
        }

        /**
         * Helper Method that sets the calling parameters used to call forth the method that is being analyzed.
         */
        private def getCallingParameters(param: Int,
                                         domain: AnalysisDomain): (domain.IntegerRange, domain.IntegerRange) = {
            param match {
                case 1  ⇒ (domain.positive, domain.negative)
                case 2  ⇒ (domain.negative, domain.positive)
                case 3  ⇒ (domain.positiveVal, domain.negativeVal)
                case 4  ⇒ (domain.negativeVal, domain.positiveVal)
                case 5  ⇒ (domain.positive, domain.zero)
                case 6  ⇒ (domain.zero, domain.positive)
                case 7  ⇒ (domain negative, domain.zero)
                case 8  ⇒ (domain.zero, domain.negative)
                case 9  ⇒ (domain.intMax, domain.intMax)
                case 10 ⇒ (domain.intMin, domain.intMin)
                case 11 ⇒ (domain.completeIntegerRange, domain.completeIntegerRange)
                case _  ⇒ (domain.zero, domain.zero)
            }
        }

        /*
         * Creates parameter mappings for a method based on the pattern: 
         * Assuming the method has three integer parameter(a, b, c) => 
         * a is being set to a concrete parameter value, while b and c stay AnInteger.
         * That is being done for each parameter of the method.
         */
        private def createParameterIndependentMapping(domain: AnalysisDomain,
                                                      parameterValue: AnalysisDomain#IntRange, parameterPosition: Int)(defaultLocals: domain.Locals): domain.Locals = {
            var offset = parameterPosition
            var mappedLocals = defaultLocals
            for (
                j ← 0 until defaultLocals.size if (defaultLocals(j).isInstanceOf[domain.AnIntegerValue] || defaultLocals(j).isInstanceOf[domain.IntegerRange])
            ) {
                if (offset == 0) {
                    mappedLocals = mappedLocals.asInstanceOf[domain.Locals].updated(j, parameterValue.asInstanceOf[domain.DomainValue])
                } else {
                    mappedLocals = mappedLocals.asInstanceOf[domain.Locals].updated(j, domain.AnIntegerValue)
                }
                offset = offset - 1
            }
            mappedLocals
        }

        /**
         * Method to map the current integer configuration onto the locals used by the abstract interpretation.
         *
         * @param domain The current domain.
         * @param listOfParameters The integer permutation to be mapped.
         * @param defaultLocals Some default locals.
         */
        private def mapLocals(domain: AnalysisDomain,
                              listOfParameters: List[AnalysisDomain#IntRange])(defaultLocals: domain.Locals): domain.Locals = {
            var parameter = listOfParameters.head
            val paramIterator = listOfParameters.iterator
            for (
                i ← 0 until defaultLocals.size if (defaultLocals(i).isInstanceOf[domain.AnIntegerValue])
            ) {
                defaultLocals.update(i, parameter.asInstanceOf[domain.DomainValue])
                if (paramIterator.hasNext) parameter = paramIterator.next()
            }
            defaultLocals
        }

        /**
         * Returns a map, mapping the used parameters(as operands) to potentially caused exceptions.
         */
        private def getMappedOperandsToExceptions(domain: AnalysisDomain)(collectedInformation: CollectedInformation): scala.collection.immutable.Map[ValuesDomain#Operands, scala.collection.immutable.Set[ValuesDomain#ExceptionValue]] = {
            def localsToOperands(domain: AnalysisDomain)(locals: domain.Locals): domain.Operands = {
                val op: domain.Operands = locals.toSeq.filterNot(x ⇒ x == null).reverse.toList
                op
            }
            // if (listEntry._2._1 != null)
            val mapOfExceptionsWithCorrespondingParameter: Map[AnalysisDomain#Operands, Set[org.opalj.ai.ValuesDomain#ExceptionValue]] =
                (collectedInformation.map(entry ⇒
                    if (entry._4.nonEmpty) {
                        (localsToOperands(domain)(entry._5.asInstanceOf[domain.Locals])
                            -> adaptExceptions(domain, entry._1, entry._4).values.flatten.toSet)
                    } else {
                        (localsToOperands(domain)(entry._5.asInstanceOf[domain.Locals])
                            -> Set.empty)
                    }).asInstanceOf[List[(AnalysisDomain#Operands, Set[org.opalj.ai.ValuesDomain#ExceptionValue])]].toMap)
                    .filterNot(entry ⇒ entry._2.isEmpty)
            mapOfExceptionsWithCorrespondingParameter.asInstanceOf[scala.collection.immutable.Map[ValuesDomain#Operands, scala.collection.immutable.Set[ValuesDomain#ExceptionValue]]]
        }

        /**
         *  Adapts exceptions to a common domain.
         */
        private def adaptExceptions(targetDomain: AnalysisDomain,
                                    originDomain: AnalysisDomain,
                                    exceptions: Map[PC, AnalysisDomain#Exceptions]): Map[PC, targetDomain.ThrownException] = {

            exceptions.toList.map(listEntry ⇒
                (listEntry._1, listEntry._2.map(exception ⇒
                    exception.asInstanceOf[originDomain.DomainSingleOriginReferenceValue].adapt(
                        targetDomain,
                        exception.asInstanceOf[originDomain.DomainSingleOriginReferenceValue].origin)).toSet))
                .asInstanceOf[List[(PC, targetDomain.ThrownException)]].toMap
        }

        /**
         * Creates a nice string representation of derived knowledge out of the collected information
         *
         * @param classFile The declaring classfile.
         * @param method The recently analyzed method.
         * @param domain The most recently used domain.
         * @param collectedInformation The collected information during the analysis.
         */
        private def createReport(classFile: ClassFile,
                                 method: Method,
                                 domain: AnalysisDomain)(collectedInformation: CollectedInformation): (String, Option[ValuesDomain#DomainValue], Int) = {
            import Console._

            /*
             *  adapt integerRange to a common domain
             */
            def adaptIntRange(targetDomain: AnalysisDomain,
                              intRange: AnalysisDomain#IntRange): targetDomain.IntegerRange = {
                targetDomain.IntegerRange(intRange.lowerBound, intRange.upperBound)
            }

            /*
             * Create a String containing information derived from the collected 
             * results regarding the possible return values/ranges of the analysed method.
             */
            def createValuesReport(): (String, Option[List[domain.IntegerRange]], Option[domain.IntegerRange]) = {
                // adapt IntegerRanges to one domain
                val listOfIntegerRanges = collectedInformation.map(listEntry ⇒
                    listEntry._3.map(intVal ⇒
                        adaptIntRange(domain, intVal.asInstanceOf[listEntry._1.IntegerRange]))).flatten.distinct
                if (listOfIntegerRanges.isEmpty) {
                    ("No recorded return Values", None, None)
                } else {
                    // test whether the method always returns the positive/negative val if its passed a negative and positive one
                    val returnsAlwaysValue =
                        if (listOfIntegerRanges.contains(domain.positiveVal) &&
                            listOfIntegerRanges.filter { _.equals(domain.positiveVal) }.size
                            >= method.descriptor.parameterTypes.filter(_.isIntegerType).size)
                            " returns always Positive Val "
                        else if (listOfIntegerRanges.contains(domain.negativeVal) &&
                            listOfIntegerRanges.filter { _.equals(domain.negativeVal) }.size
                            >= method.descriptor.parameterTypes.filter(_.isIntegerType).size)
                            " returns always Negative Val "
                        else ""

                    val returnRange =
                        domain.IntegerRange((listOfIntegerRanges.reduceLeft((l, r) ⇒
                            if (r.lowerBound < l.lowerBound) r else l)).lowerBound,
                            (listOfIntegerRanges.reduceLeft((l, r) ⇒
                                if (r.upperBound > l.upperBound) r else l)).upperBound)
                    val conclusion: String =
                        returnRange match {
                            case domain.zero                 ⇒ " returns always zero"
                            case domain.completeIntegerRange ⇒ " returns the complete integerrange"
                            case domain.plusOne              ⇒ " returns always One"
                            case domain.minusOne             ⇒ " returns always minus One"
                            case domain.positive             ⇒ " returns always positive IntegerRange"
                            case domain.negative             ⇒ " returns always negative IntegerRange"
                            case _                           ⇒ ""
                        }
                    (listOfIntegerRanges + BLUE+" possible Return Range: "+returnRange +
                        returnsAlwaysValue + GREEN + conclusion + RESET, Option(listOfIntegerRanges), Option(returnRange))
                }
            }

            /*
             * Creates a nice String containing information derived from the collected results 
             * regarding the possible parameter-ranges causing exceptions of the analyzed method.
             */
            def createExceptionReport(): (String, Option[domain.IntegerRange], Option[Map[PC, domain.ThrownException]]) = {
                // analyzing the thrown exceptions:
                val listOfExceptionCausingParameterRanges: List[domain.IntegerRange] =
                    collectedInformation.map(listEntry ⇒
                        if (listEntry._4.nonEmpty) {
                            if (method.descriptor.parameterTypes.exists { _.isIntegerType }) {
                                List(adaptIntRange(domain, listEntry._2._1.asInstanceOf[listEntry._1.IntegerRange]),
                                    adaptIntRange(domain, listEntry._2._2.asInstanceOf[listEntry._1.IntegerRange]))
                                    .asInstanceOf[List[domain.IntegerRange]]
                            } else if (listEntry._2._1 != null) {
                                List(adaptIntRange(domain, listEntry._2._1.asInstanceOf[listEntry._1.IntegerRange]))
                                    .asInstanceOf[List[domain.IntegerRange]]
                            } else {
                                List.empty.asInstanceOf[List[domain.IntegerRange]]
                            }
                        } else List.empty.asInstanceOf[List[domain.IntegerRange]]).flatten
                val listOfListsOfExceptions = collectedInformation.map(listEntry ⇒
                    if (listEntry._4.nonEmpty) {
                        adaptExceptions(domain, listEntry._1, listEntry._4)
                    } else
                        Map.empty.asInstanceOf[Map[PC, domain.ThrownException]])
                val mergedExceptions = listOfListsOfExceptions.reduceLeft((l, r) ⇒
                    l ++ r.map {
                        case (pc, ex) ⇒
                            pc -> (ex ++ l.getOrElse(pc, Set.empty.asInstanceOf[domain.ThrownException]))
                    })
                // find lowest lowerbound and highest upperbound of the parameters responsible for causing exceptions
                val exceptionRange =
                    if (listOfExceptionCausingParameterRanges.isEmpty) None
                    else
                        Option(domain.IntegerRange((listOfExceptionCausingParameterRanges.reduceLeft((l, r) ⇒
                            if (r.lowerBound < l.lowerBound) r else l)).lowerBound,
                            (listOfExceptionCausingParameterRanges.reduceLeft((l, r) ⇒
                                if (r.upperBound > l.upperBound) r else l)).upperBound))
                //if (listOfExceptionCausingParameterRanges.isEmpty || listOfListsOfExceptions.isEmpty)
                if (mergedExceptions.isEmpty)
                    (" no exceptions thrown ", exceptionRange, None)
                else
                    (BLUE+" Total Parameter Range responsible for Exceptions:  "+
                        BLUE + exceptionRange.getOrElse("No Parameter configuration available") + RED+"  Exceptions thrown: "+
                        mergedExceptions + RED + RESET, exceptionRange, Option(mergedExceptions))
            }

            val declaringClassOfMethod = classFile.thisType.toJava
            val (valuesReport, listOfIntegerRanges, returnRange) = createValuesReport
            val (exceptionReport, exceptionRange, mergedExceptions) = createExceptionReport
            val report = "Refined Integer range of "+BOLD + BLUE +
                declaringClassOfMethod+"{ "+method.toJava+" }"+
                " => "+GREEN + valuesReport + exceptionReport
            (report, returnRange.asInstanceOf[Option[ValuesDomain#DomainValue]],
                if ((listOfIntegerRanges.nonEmpty && returnRange.isDefined && returnRange.get != domain.completeIntegerRange)
                    || (exceptionRange != null && mergedExceptions.size > 0)) 1 else 0)
        }
    }
}

/**
 *  Wrapper class for the results report.
 *
 *  @param str String of the report to be displayed
 *  @param count determines if it is a "more-refined" value,
 *  eg. the potential return range is != whole integer spectrum or potential exceptions have been found.
 */
case class IntergerRefinedValue(str: String, count: Int) {
    override def toString = {
        str + (if (count > 0) " refined-value " else "")
    }
}

/**
 * Domain to be used with invocation.
 */
class SecondaryInvocationDomain(
    val project: Project[java.net.URL],
    val ai: InterruptableAI[_],
    val method: Method)
        extends CorrelationalDomain
        with TheProject
        with TheMethod
        with DefaultDomainValueBinding
        with ThrowAllPotentialExceptionsConfiguration
        with DefaultHandlingOfMethodResults
        with IgnoreSynchronization
        with domain.l0.DefaultTypeLevelFloatValues
        with domain.l0.DefaultTypeLevelDoubleValues
        with domain.l0.TypeLevelFieldAccessInstructions
        with domain.l0.TypeLevelInvokeInstructions
        with SpecialMethodsHandling
        with domain.l1.DefaultReferenceValuesBinding
        with domain.l1.DefaultClassValuesBinding
        with domain.l1.DefaultIntegerRangeValues
        with domain.l1.MaxArrayLengthRefinement
        with domain.l1.ConstraintsBetweenIntegerValues
        with domain.l1.DefaultLongValues
        with domain.l1.LongValuesShiftOperators
        with domain.l1.ConcretePrimitiveValuesConversions
        with domain.l1.LibraryMethodsHandling
        with domain.ProjectBasedClassHierarchy