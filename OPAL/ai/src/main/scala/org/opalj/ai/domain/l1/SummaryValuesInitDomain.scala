/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj
package ai
package domain
package l1

import org.opalj.br.{ Type, ObjectType, ReferenceType, FieldType, ArrayType }
import org.opalj.br.MethodDescriptor
import org.opalj.br.BootstrapMethod
import org.opalj.br.analyses.{ ClassHierarchy ⇒ DefaultClassHierarchy }
import org.opalj.br.analyses.{ Project }
import scala.reflect.ClassTag

/**
 * A very simple and basic domain to just instantiate values for summary results.
 *
 * @author Marius Diebel
 */

class DefaultSummaryValuesInitDomain
        extends SummaryValuesInitDomain
        //with DoubleValuesDomain
        //with FloatValuesDomain
        //with LongValuesDomain
        //with IntegerValuesDomain
        //with ValuesDomain
        with DefaultDomainValueBinding
        with l0.DefaultTypeLevelFloatValues
        with l0.DefaultTypeLevelDoubleValues
        //with l0.TypeLevelFieldAccessInstructions
        with l1.DefaultReferenceValuesBinding
        //with l1.DefaultClassValuesBinding
        with l1.DefaultIntegerRangeValues
        with l1.DefaultLongValues
        //with l1.LongValuesShiftOperators
        //with l1.ConcretePrimitiveValuesConversions
        with TypedValuesFactory
        //with ExceptionsFactory
        with ClassHierarchy {
}

trait SummaryValuesInitDomain extends CorrelationalDomain with Configuration {

    def throwAllHandledExceptionsOnMethodCall: Boolean = true
    def throwNullPointerExceptionOnThrow: Boolean = true
    def throwNullPointerExceptionOnMethodCall: Boolean = true
    def throwNullPointerExceptionOnFieldAccess: Boolean = true
    def throwArithmeticExceptions: Boolean = true
    def throwNullPointerExceptionOnMonitorAccess: Boolean = true
    def throwIllegalMonitorStateException: Boolean = true
    def throwNullPointerExceptionOnArrayAccess: Boolean = true
    def throwArrayIndexOutOfBoundsException: Boolean = true
    def throwArrayStoreException: Boolean = true
    def throwNegativeArraySizeException: Boolean = true
    def throwClassCastException: Boolean = true
    def throwClassNotFoundException: Boolean = true

    override def invokevirtual(
        pc: PC,
        declaringClass: ReferenceType,
        name: String,
        methodDescriptor: MethodDescriptor,
        operands: Operands): MethodCallResult =
        throw new UnsupportedOperationException

    override def invokeinterface(
        pc: PC,
        declaringClass: ObjectType,
        name: String,
        methodDescriptor: MethodDescriptor,
        operands: Operands): MethodCallResult =
        throw new UnsupportedOperationException

    override def invokespecial(
        pc: PC,
        declaringClass: ObjectType,
        name: String,
        methodDescriptor: MethodDescriptor,
        operands: Operands): MethodCallResult =
        throw new UnsupportedOperationException

    override def invokestatic(
        pc: PC,
        declaringClass: ObjectType,
        name: String,
        methodDescriptor: MethodDescriptor,
        operands: Operands): MethodCallResult =
        throw new UnsupportedOperationException

    override def invokedynamic(
        pc: PC,
        bootstrapMethod: BootstrapMethod,
        name: String,
        methodDescriptor: MethodDescriptor,
        operands: Operands): Computation[DomainValue, ExceptionValues] =
        throw new UnsupportedOperationException

    override def getstatic(
        pc: PC,
        declaringClass: ObjectType,
        name: String,
        fieldType: FieldType): Computation[DomainValue, Nothing] =
        throw new UnsupportedOperationException

    override def monitorenter(
        pc: PC,
        value: DomainValue): Computation[Nothing, ExceptionValue] =
        throw new UnsupportedOperationException

    override def monitorexit(
        pc: PC,
        value: DomainValue): Computation[Nothing, ExceptionValue] =
        throw new UnsupportedOperationException

    override def returnVoid(pc: PC): Unit =
        throw new UnsupportedOperationException

    override def abruptMethodExecution(pc: PC, exception: ExceptionValue): Unit =
        throw new UnsupportedOperationException

    override def areturn(pc: PC, value: DomainValue): Unit =
        throw new UnsupportedOperationException

    override def dreturn(pc: PC, value: DomainValue): Unit =
        throw new UnsupportedOperationException

    override def freturn(pc: PC, value: DomainValue): Unit =
        throw new UnsupportedOperationException

    override def ireturn(pc: PC, value: DomainValue): Unit =
        throw new UnsupportedOperationException

    override def lreturn(pc: PC, value: DomainValue): Unit =
        throw new UnsupportedOperationException

    override def l2d(pc: PC, value: DomainValue): DomainValue =
        throw new UnsupportedOperationException
    override def l2f(pc: PC, value: DomainValue): DomainValue =
        throw new UnsupportedOperationException
    override def l2i(pc: PC, value: DomainValue): DomainValue =
        throw new UnsupportedOperationException

    override def i2d(pc: PC, value: DomainValue): DomainValue =
        throw new UnsupportedOperationException
    override def i2f(pc: PC, value: DomainValue): DomainValue =
        throw new UnsupportedOperationException
    override def i2l(pc: PC, value: DomainValue): DomainValue =
        throw new UnsupportedOperationException

    override def f2d(pc: PC, strictfp: Boolean, value: DomainValue): DomainValue =
        throw new UnsupportedOperationException
    override def f2i(pc: PC, value: DomainValue): DomainValue =
        throw new UnsupportedOperationException
    override def f2l(pc: PC, value: DomainValue): DomainValue =
        throw new UnsupportedOperationException

    override def d2f(pc: PC, strictfp: Boolean, value: DomainValue): DomainValue =
        throw new UnsupportedOperationException
    override def d2i(pc: PC, value: DomainValue): DomainValue =
        throw new UnsupportedOperationException
    override def d2l(pc: PC, value: DomainValue): DomainValue =
        throw new UnsupportedOperationException

    override def lshl(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        throw new UnsupportedOperationException

    override def lshr(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        throw new UnsupportedOperationException

    override def lushr(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        throw new UnsupportedOperationException

    override def ReferenceValue(origin: Int, referenceType: org.opalj.br.ReferenceType): DomainReferenceValue = throw new UnsupportedOperationException

    def dadd(
        pc: PC,
        strictfp: Boolean,
        value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException

    def ddiv(
        pc: PC,
        strictfp: Boolean,
        value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException

    def dmul(
        pc: PC,
        strictfp: Boolean,
        value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException

    def drem(
        pc: PC,
        strictfp: Boolean,
        value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException

    def dsub(
        pc: PC,
        strictfp: Boolean,
        value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException

    // Members declared in org.opalj.ai.DoubleValuesFactory
    def DoubleValue(origin: Int, value: Double): SummaryValuesInitDomain.this.DomainValue = throw new UnsupportedOperationException
    def DoubleValue(origin: Int): SummaryValuesInitDomain.this.DomainValue = throw new UnsupportedOperationException

    // Members declared in org.opalj.ai.ExceptionsFactory
    def ArithmeticException(origin: Int): SummaryValuesInitDomain.this.ExceptionValue = throw new UnsupportedOperationException
    def ArrayIndexOutOfBoundsException(origin: Int): SummaryValuesInitDomain.this.ExceptionValue = throw new UnsupportedOperationException
    def ArrayStoreException(origin: Int): SummaryValuesInitDomain.this.ExceptionValue = throw new UnsupportedOperationException
    def ClassCastException(origin: Int): SummaryValuesInitDomain.this.ExceptionValue = throw new UnsupportedOperationException
    def ClassNotFoundException(origin: Int): SummaryValuesInitDomain.this.ExceptionValue = throw new UnsupportedOperationException
    def NegativeArraySizeException(origin: Int): SummaryValuesInitDomain.this.ExceptionValue = throw new UnsupportedOperationException
    def NullPointerException(origin: Int): SummaryValuesInitDomain.this.ExceptionValue = throw new UnsupportedOperationException
    def Throwable(origin: Int): SummaryValuesInitDomain.this.ExceptionValue = throw new UnsupportedOperationException

    //
    // BINARY ARITHMETIC EXPRESSIONS
    //
    def fadd(
        pc: PC,
        strictfp: Boolean,
        value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException

    def fdiv(
        pc: PC,
        strictfp: Boolean,
        value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException

    def fmul(
        pc: PC,
        strictfp: Boolean,
        value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException

    def frem(
        pc: PC,
        strictfp: Boolean,
        value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException

    def fsub(
        pc: PC,
        strictfp: Boolean,
        value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException
    // Members declared in org.opalj.ai.FloatValuesFactory
    def FloatValue(origin: Int, value: Float): SummaryValuesInitDomain.this.DomainValue = throw new UnsupportedOperationException
    def FloatValue(origin: Int): SummaryValuesInitDomain.this.DomainValue = throw new UnsupportedOperationException

    // Members declared in org.opalj.ai.LongValuesDomain
    def ladd(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException
    def lsub(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException

    def ldiv(pc: PC, value1: DomainValue, value2: DomainValue): LongValueOrArithmeticException = throw new UnsupportedOperationException

    def lmul(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException

    def lrem(pc: PC, value1: DomainValue, value2: DomainValue): LongValueOrArithmeticException = throw new UnsupportedOperationException

    def land(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException

    def lor(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException

    def lxor(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException

    // Members declared in org.opalj.ai.LongValuesFactory
    def LongValue(origin: Int, value: Long): SummaryValuesInitDomain.this.DomainValue = throw new UnsupportedOperationException
    def LongValue(origin: Int): SummaryValuesInitDomain.this.DomainValue = throw new UnsupportedOperationException

    // Members declared in org.opalj.ai.ReferenceValuesDomain
    //    override def aaload(pc: PC, index: DomainValue, arrayref: DomainValue): ArrayLoadResult = throw new UnsupportedOperationException
    //
    //    override def aastore(
    //        pc: PC,
    //        value: DomainValue,
    //        index: DomainValue,
    //        arrayref: DomainValue): ArrayStoreResult = throw new UnsupportedOperationException
    //
    //    override def baload(pc: PC, index: DomainValue, arrayref: DomainValue): ArrayLoadResult = throw new UnsupportedOperationException
    //
    //    override def bastore(
    //        pc: PC,
    //        value: DomainValue,
    //        index: DomainValue,
    //        arrayref: DomainValue): ArrayStoreResult = throw new UnsupportedOperationException
    //
    //    override def caload(pc: PC, index: DomainValue, arrayref: DomainValue): ArrayLoadResult = throw new UnsupportedOperationException
    //
    //    override def castore(
    //        pc: PC,
    //        value: DomainValue,
    //        index: DomainValue,
    //        arrayref: DomainValue): ArrayStoreResult = throw new UnsupportedOperationException
    //
    //    override def daload(pc: PC, index: DomainValue, arrayref: DomainValue): ArrayLoadResult = throw new UnsupportedOperationException
    //
    //    override def dastore(
    //        pc: PC,
    //        value: DomainValue,
    //        index: DomainValue,
    //        arrayref: DomainValue): ArrayStoreResult = throw new UnsupportedOperationException
    //
    //    override def faload(pc: PC, index: DomainValue, arrayref: DomainValue): ArrayLoadResult = throw new UnsupportedOperationException
    //
    //    override def fastore(
    //        pc: PC,
    //        value: DomainValue,
    //        index: DomainValue,
    //        arrayref: DomainValue): ArrayStoreResult = throw new UnsupportedOperationException
    //
    //    override def iaload(pc: PC, index: DomainValue, arrayref: DomainValue): ArrayLoadResult = throw new UnsupportedOperationException
    //
    //    override def iastore(
    //        pc: PC,
    //        value: DomainValue,
    //        index: DomainValue,
    //        arrayref: DomainValue): ArrayStoreResult = throw new UnsupportedOperationException
    //
    //    override def laload(pc: PC, index: DomainValue, arrayref: DomainValue): ArrayLoadResult = throw new UnsupportedOperationException
    //
    //    override def lastore(
    //        pc: PC,
    //        value: DomainValue,
    //        index: DomainValue,
    //        arrayref: DomainValue): ArrayStoreResult = throw new UnsupportedOperationException
    //
    //    override def saload(pc: PC, index: DomainValue, arrayref: DomainValue): ArrayLoadResult = throw new UnsupportedOperationException
    //
    //    override def sastore(
    //        pc: PC,
    //        value: DomainValue,
    //        index: DomainValue,
    //        arrayref: DomainValue): ArrayStoreResult = throw new UnsupportedOperationException
    // Members declared in org.opalj.ai.ReferenceValuesFactory
    def ClassValue(origin: org.opalj.ai.ValueOrigin, t: org.opalj.br.Type): SummaryValuesInitDomain.this.DomainReferenceValue = throw new UnsupportedOperationException
    def InitializedArrayValue(origin: org.opalj.ai.ValueOrigin, arrayType: org.opalj.br.ArrayType, counts: List[Int]): SummaryValuesInitDomain.this.DomainReferenceValue = throw new UnsupportedOperationException
    def InitializedObjectValue(origin: org.opalj.ai.ValueOrigin, objectType: org.opalj.br.ObjectType): SummaryValuesInitDomain.this.DomainReferenceValue = throw new UnsupportedOperationException
    def NewObject(origin: org.opalj.ai.ValueOrigin, objectType: org.opalj.br.ObjectType): SummaryValuesInitDomain.this.DomainReferenceValue = throw new UnsupportedOperationException
    def NonNullObjectValue(origin: org.opalj.ai.ValueOrigin, objectType: org.opalj.br.ObjectType): SummaryValuesInitDomain.this.DomainReferenceValue = throw new UnsupportedOperationException
    def NullValue(origin: org.opalj.ai.ValueOrigin): SummaryValuesInitDomain.this.DomainReferenceValue = throw new UnsupportedOperationException
    def StringValue(origin: org.opalj.ai.ValueOrigin, value: String): SummaryValuesInitDomain.this.DomainReferenceValue = throw new UnsupportedOperationException

    // Members declared in org.opalj.ai.DoubleValuesDomain
    override def dcmpg(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException

    override def dcmpl(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException

    override def dneg(pc: PC, strictfp: Boolean, value: DomainValue): DomainValue = throw new UnsupportedOperationException

    // Members declared in org.opalj.ai.FloatValuesDomain
    override def fcmpg(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException
    override def fcmpl(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException
    override def fneg(pc: PC, strictfp: Boolean, value: DomainValue): DomainValue = throw new UnsupportedOperationException

    // Members declared in org.opalj.ai.LongValuesDomain
    override def lcmp(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = throw new UnsupportedOperationException
    override def lneg(pc: PC, value: DomainValue): DomainValue = throw new UnsupportedOperationException

    // Members declared in org.opalj.ai.ReferenceValuesDomain
    override def arraylength(
        pc: PC,
        arrayref: DomainValue): Computation[DomainValue, ExceptionValue] = throw new UnsupportedOperationException
    override def isValueSubtypeOf(
        value: DomainValue,
        supertype: ReferenceType): Answer = throw new UnsupportedOperationException
    override def refIsNull(pc: PC, value: DomainValue): Answer = throw new UnsupportedOperationException
    override def refAreEqual(pc: PC, value1: DomainValue, value2: DomainValue): Answer = throw new UnsupportedOperationException
    //    override def refSetUpperBound(
    //        pc: PC,
    //        bound: ReferenceType,
    //        operands: Operands,
    //        locals: Locals): (Operands, Locals) = throw new UnsupportedOperationException
    override def newarray(
        pc: PC,
        count: DomainValue,
        componentType: FieldType): Computation[DomainValue, ExceptionValue] = throw new UnsupportedOperationException
    override def multianewarray(
        pc: PC,
        counts: List[DomainValue],
        arrayType: ArrayType): Computation[DomainValue, ExceptionValue] = throw new UnsupportedOperationException

    def classHierarchy: DefaultClassHierarchy = org.opalj.br.analyses.ClassHierarchy.preInitializedClassHierarchy
    // override val DomainReferenceValue: ClassTag[DomainReferenceValue] = null

    override def putfield(
        pc: PC,
        objectref: DomainValue,
        value: DomainValue,
        declaringClass: ObjectType,
        fieldName: String,
        fieldType: FieldType): Computation[Nothing, ExceptionValue] =
        throw new UnsupportedOperationException

    override def putstatic(
        pc: PC,
        value: DomainValue,
        declaringClass: ObjectType,
        fieldName: String,
        fieldType: FieldType): Computation[Nothing, Nothing] = throw new UnsupportedOperationException

    override def getfield(
        pc: PC,
        objectref: DomainValue,
        declaringClass: ObjectType,
        fieldName: String,
        fieldType: FieldType): Computation[DomainValue, ExceptionValue] = throw new UnsupportedOperationException

}

