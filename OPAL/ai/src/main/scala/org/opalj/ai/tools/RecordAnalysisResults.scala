/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj
package ai
package tools

import org.opalj.br.Method
import org.opalj.br.MethodDescriptor
import org.opalj.br.ObjectType
import org.opalj.ai.ValuesDomain
import org.opalj.ai.Configuration
import org.opalj.ai.IntegerValuesDomain
import org.opalj.ai.ReferenceValuesDomain
import scala.collection.mutable.AnyRefMap
import org.opalj.ai.domain._

/**
 * Basic methods to register and write out analysis results.
 *
 * @author Marius Diebel
 */
trait RecordAnalysisResults {
    callingDomain: ValuesFactory with ReferenceValuesDomain with ValuesDomain with IntegerValuesDomain with domain.l1.DefaultReferenceValuesBinding with domain.l1.DefaultIntegerRangeValues with Configuration with TheCode ⇒

    import RecordAnalysisResults._

    // we need quite a number of types since the type-class to create scalacode can't access the domain but needs these types
    type ThrownExceptions = Set[ValuesDomain#ExceptionValue]
    type RegisteredResults = scala.collection.mutable.AnyRefMap[(Method, ObjectType), (Option[ValuesDomain#DomainValue], Map[ValuesDomain#Operands, Set[ValuesDomain#ExceptionValue]])]
    type RegisteredExceptions = (org.opalj.ai.ValuesDomain#Operands, Set[ValuesDomain#ExceptionValue])
    type RegisteredResultsEntry = ((Method, ObjectType), (Option[ValuesDomain#DomainValue], Map[ValuesDomain#Operands, Set[ValuesDomain#ExceptionValue]]))
    type IntegerRangeValue = callingDomain.IntegerRange
    type IntVal = callingDomain.AnIntegerValue
    type DomainVal = callingDomain.DomainValue
    type RefVal = callingDomain.DomainReferenceValue
    type SingleOriginRefVal = callingDomain.DomainSingleOriginReferenceValue

    // reference to basic file operations class to handle the writing to a file
    private val fileOperator = FileOperations

    /**
     * Adds the summary information for a method to the record
     *
     * @param method The method for which information should be stored
     * @param expectedResult The expected return value for the method, can be empty.
     * @param exceptions A map containing parameters and potential exceptions that occurred under these parameters.
     * @param declaringClass The declaring class of the Method.
     */
    def registerMethod(method: Method,
                       expectedResult: Option[ValuesDomain#DomainValue],
                       exceptions: Map[ValuesDomain#Operands, Set[ValuesDomain#ExceptionValue]],
                       declaringClass: ObjectType): Unit = {

        registeredResults.synchronized {
            registeredResults += ((method, declaringClass) -> ((expectedResult, exceptions)))
        }
    }

    /**
     * Returns the desired summary-name which is also used as the filename eventually.
     * Needs to be implemented by the analysis.
     */
    def getSummaryName(): String

    /**
     * Method to initiate the write-out of a generated source file containing the summary results.
     */
    def writeOutResults() = {
        fileOperator.writeOut(getSummaryName, createGeneratedSource().toString())
    }

    /**
     * Print out the current amount of registered methods to evaluate progress and debug
     */
    def printRegisteredResultsSize(): Unit = {
        println(registeredResults.size)
    }

    private def printInitTimerForSummary(): Boolean = true

    /**
     * Returns valid scala-code as string for certain elements, making use of the ToScalaCode Type-Class.
     */
    private def makeScalaCode[T](sobject: T)(implicit toScalaCode: ToScalaCode[T]): String =
        toScalaCode.makeCode(sobject)

    /**
     *  Turns a single entry for a method into scala code
     */
    private def entryToStringBuffer(e: RegisteredResultsEntry): StringBuffer = {
        val qM = "\""
        val str = new StringBuffer
        str.append(s"results += (((${makeScalaCode(e._1._2)}, ${qM}${e._1._1.name}${qM}," +
            s" ${makeScalaCode(e._1._1.descriptor)}))" +
            s" -> ((${makeScalaCode(e._2._1)}, \n ${makeScalaCode(e._2._2)})))\n")
    }

    /**
     * Takes an entry which is too big(>64k) for a single method and splits the entry into separate update methods.
     *
     * @param e The entry that needs to be split apart.
     */
    private def initMethodTooLarge(e: RegisteredResultsEntry): StringBuffer = {
        val qM = "\""
        // the method signature of the method that needs to be added
        val keytriple = ("(" + makeScalaCode(e._1._2) + ", " + qM + e._1._1.name + qM +
            ", " + makeScalaCode(e._1._1.descriptor) + ")")
        val emptyExceptionMap = "Map.empty.asInstanceOf[Map[OperandsList, Set[ExceptionValue]]]"
        val resultReturnValue = makeScalaCode(e._2._1)
        val returnString = new StringBuffer
        val submethods = new StringBuffer
        var numberofsubinits = 1

        /*
         * Helper Method that creates an update for a pair exceptions to operands
         */
        def createValueUpdate(entry: RegisteredExceptions): StringBuffer = {
            val vstr = new StringBuffer
            //vstr.append("results.synchronized{")
            vstr.append(s"val res = results.get(${keytriple}).getOrElse((${resultReturnValue}, " +
                s"${emptyExceptionMap}))._2 + ${makeScalaCode(entry)} \n")
            vstr.append(s"results.update(${keytriple}, (${resultReturnValue}, " +
                "res.asInstanceOf[Map[OperandsList, Set[ExceptionValue]]]))")
            //vstr.append("\n}") //closing in synchronized block
            vstr
        }

        e._2._2.foreach { x ⇒
            submethods.append(s"def ${getSummaryName}subinit${numberofsubinits}(): Unit = { \n")
            val parsedUpdate = createValueUpdate(x)
            if (parsedUpdate.length() < 42000)
                submethods.append(parsedUpdate)
            else
                submethods.append("")
            submethods.append("\n } \n")
            numberofsubinits += 1
        }
        for (i ← 1 until numberofsubinits) {
            returnString.append(s"${getSummaryName}subinit${i}()\n")
        }
        returnString.append(submethods)
        returnString
    }

    /**
     * Creates the init method which calls all the subinit methods.
     */
    private def createInitMethod(): StringBuffer = {
        val initMethods = createSubInitMethods
        val str1 = new StringBuffer
        // Init Method Header
        str1.append("override def init(domain: DefaultSummaryValuesInitDomain): SummaryMapType = { \n")
        // assign init domain
        str1.append("d = domain \n")
        // append the calls to the created sub init methods
        if (printInitTimerForSummary) str1.append("time{ \n")
        for (i ← 0 until initMethods._2) {
            str1.append(s"${getSummaryName}init${i}()\n")
        }
        if (printInitTimerForSummary) str1.append(" \n } { t ⇒ println(\"Init time" +
            s" of ${getSummaryName}" + " \"+t.toSeconds) } \n")
        str1.append("return results")
        str1.append("} \n \n \n")
        // append the initMethods bodies
        str1.append(initMethods._1)
    }

    /**
     * Iterates over each entry and splits the result into appropriate sized init methods.
     */
    private def createSubInitMethods(): (StringBuffer, Int) = {
        var index = 1
        var offset = 0
        val str = new StringBuffer

        // HelperMethod to perform the append of new entries
        def appendMethodHeader(): Unit = {
            str.append("} \n \n")
            str.append(s" def ${getSummaryName}init${index}(): Unit = { \n")
            index += 1
            offset = str.length
        }

        //create
        //str.append(s"def ${getSummaryName}init0(d: DefaultSummaryValuesInitDomain): Unit = {")
        str.append(s"def ${getSummaryName}init0(): Unit = {")
        registeredResults.foreach { e ⇒
            val parsedResultEntry = entryToStringBuffer(e)
            // split init into subsubmethods because the entry is larger than 64k
            if ((str.length() - offset) > 0 &&
                ((str.length() - offset) + parsedResultEntry.length()) >= 32000) {
                appendMethodHeader
                str.append(initMethodTooLarge(e))
            } else {
                // a method needs to stay below 64k in size
                appendMethodHeader
                str.append(parsedResultEntry)
            }
        }
        str.append("}")

        (str, index)
    }

    /**
     * Generates the entire source contained in a StringBuffer.
     */
    private def createGeneratedSource(): StringBuffer = {
        val str = new StringBuffer
        str.append(s"// generated summary - source file for project: ${getSummaryName}\n")
        str.append("package org.opalj \n package ai \n package domain \n package summaries \n")
        if (printInitTimerForSummary) str.append("import org.opalj.util.PerformanceEvaluation.time \n")
        str.append("import org.opalj.br.MethodDescriptor \n" +
            "import org.opalj.br.ObjectType \n" +
            "import org.opalj.br.IntegerType \n" +
            "import scala.collection.mutable.AnyRefMap \n" +
            "import l1.DefaultSummaryValuesInitDomain \n" +
            "import org.opalj.br.ArrayType \n" + " \n \n")
        str.append(s"class ${getSummaryName} extends ASummary { \n")
        str.append("val results: SummaryMapType = AnyRefMap.empty \n")
        str.append("var d: DefaultSummaryValuesInitDomain = null \n")
        str.append(createInitMethod)
        str.append("\n}")
        str
    }
}

/**
 * Companion object holding the result map to be accessible by different domain instances.
 */
object RecordAnalysisResults {
    type ThrownExceptions = Set[org.opalj.ai.ValuesDomain#ExceptionValue]
    // key: (method-signature, declaringclass) value: (return-value-range, map(ListOfParameters, SetOfExceptions))
    type RegisteredResults = AnyRefMap[(Method, ObjectType), (Option[org.opalj.ai.ValuesDomain#DomainValue], Map[org.opalj.ai.ValuesDomain#Operands, Set[org.opalj.ai.ValuesDomain#ExceptionValue]])]
    private val registeredResults: RegisteredResults = AnyRefMap.empty
}