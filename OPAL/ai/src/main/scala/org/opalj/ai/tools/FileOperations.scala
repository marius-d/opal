/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.ai.tools

/**
 * Provides basic operations to write out text(in this case source code) into a file,
 * which is placed within an opal folder in the users home folder.
 *
 * @author Marius Diebel
 */

object FileOperations {
    import java.io._
    import java.util.Properties
    import scala.language.reflectiveCalls
    import scala.collection.JavaConverters._
    import scala.language.implicitConversions

    val fileEnding = ".scala"
    val separator = File.separator
    val folderPath = userHomeDirectory + separator+".opal"

    def userHomeDirectory = System.getProperty("user.home")

    def createFolder = {
        val folder = new File(folderPath)
        if (!folder.exists())
            folder.mkdir()
    }

    def writeOut(projectName: String, scalaCode: String): Unit = {
        createFolder
        val file = new File(folderPath + separator + projectName + fileEnding)
        writeStringToFile(file, scalaCode, false)
    }

    //    def printToFile(f: java.io.File)(op: java.io.PrintWriter ⇒ Unit): Unit = {
    //        val p = new java.io.PrintWriter(f)
    //        try { op(p) } finally { p.close() }
    //    }

    def using[A <: { def close(): Unit }, B](resource: A)(f: A ⇒ B): B =
        try f(resource) finally resource.close()

    def writeStringToFile(file: File, data: String, appending: Boolean = false) =
        using(new FileWriter(file, appending))(_.write(data))

    /*
     * Load the properties file either from the specified environment variable
     * or from the default file, which is located in userHomeFolder/.opal/default.properties
     */
    def getSummaryPropertiesFileContent(): scala.collection.mutable.Map[String, String] = {
        val path = sys.props.getOrElse("org.opalj.summaries.SummaryFileLocation",
            (System.getProperty("user.home") +
                File.separator+".opal"+File.separator+"default.properties"))
        val summaryProperties: Properties = new Properties
        if (new File(path).exists()) {
            summaryProperties.load(new FileInputStream(path))
            return summaryProperties.asScala
        } else {
            scala.collection.mutable.Map.empty
        }
    }
}
