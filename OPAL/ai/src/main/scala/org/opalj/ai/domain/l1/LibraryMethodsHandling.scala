/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.opalj
package ai
package domain
package l1

import org.opalj.br.MethodDescriptor
import org.opalj.br.Method
import org.opalj.br.BootstrapMethod
import org.opalj.br.MethodDescriptor
import org.opalj.br.ObjectType
import org.opalj.br.ReferenceType
import org.opalj.br.VoidType
import org.opalj.br.IntegerType
import org.opalj.br.BootstrapMethod
import org.opalj.util.PerformanceEvaluation.time
import org.opalj.br.ObjectType.Object
import scala.collection.mutable.AnyRefMap
import org.opalj.br.analyses.Project
import org.opalj.ai.domain.summaries.ASummary

/**
 *  Provides functionality for the reuse of generated summary information.
 *
 * @author Marius Diebel
 */
trait LibraryMethodsHandling extends MethodCallsHandling {
    callingDomain: CorrelationalDomain with TheMethod with DefaultDomainValueBinding with l0.DefaultTypeLevelFloatValues with l0.DefaultTypeLevelDoubleValues with l1.DefaultReferenceValuesBinding with l1.DefaultIntegerRangeValues with l1.DefaultLongValues with TypedValuesFactory with ClassHierarchy ⇒
    import LibraryMethodsHandling._

    type SummaryMapType = AnyRefMap[(ObjectType, String, MethodDescriptor), (Option[org.opalj.ai.ValuesDomain#DomainValue], Map[org.opalj.ai.ValuesDomain#Operands, Set[org.opalj.ai.ValuesDomain#ExceptionValue]])]
    type DomainType = CorrelationalDomain with TheMethod with DefaultDomainValueBinding with l0.DefaultTypeLevelFloatValues with l0.DefaultTypeLevelDoubleValues with l1.DefaultReferenceValuesBinding with l1.DefaultIntegerRangeValues with l1.DefaultLongValues with TypedValuesFactory with ClassHierarchy
    type StoredExceptionMap = Map[org.opalj.ai.ValuesDomain#Operands, Set[org.opalj.ai.ValuesDomain#ExceptionValue]]

    // we need the originDomain, which equals the domain used to initiate the summary information
    val originDomain: DomainType = initDomain.asInstanceOf[DomainType]

    abstract override def invokestatic(
        pc: PC,
        declaringClass: ObjectType,
        name: String,
        methodDescriptor: MethodDescriptor,
        operands: Operands): MethodCallResult = {

        val methodReturn = lookup(declaringClass, name, methodDescriptor, operands)
        methodReturn.getOrElse(
            super.invokestatic(pc, declaringClass, name, methodDescriptor, operands))
    }

    abstract override def invokespecial(
        pc: PC,
        declaringClass: ObjectType,
        name: String,
        methodDescriptor: MethodDescriptor,
        operands: Operands): MethodCallResult = {

        val methodReturn = lookup(declaringClass, name, methodDescriptor, operands)
        methodReturn.getOrElse(
            super.invokespecial(pc, declaringClass, name, methodDescriptor, operands))
    }

    abstract override def invokeinterface(
        pc: PC,
        declaringClass: ObjectType,
        name: String,
        methodDescriptor: MethodDescriptor,
        operands: Operands): MethodCallResult = {

        val methodReturn = lookup(declaringClass, name, methodDescriptor, operands)
        methodReturn.getOrElse(
            super.invokeinterface(pc, declaringClass, name, methodDescriptor, operands))
    }

    abstract override def invokevirtual(
        pc: PC,
        declaringClass: ReferenceType, // e.g., Array[] x = ...; x.clone()
        name: String,
        methodDescriptor: MethodDescriptor,
        operands: Operands): MethodCallResult = {

        val declaringClassObjectType =
            if (declaringClass.isObjectType)
                declaringClass.asObjectType
            else if (declaringClass.asArrayType.elementType.isObjectType)
                declaringClass.asArrayType.elementType.asObjectType
            else
                return super.invokevirtual(pc, declaringClass, name, methodDescriptor, operands)
        val methodReturn = lookup(declaringClassObjectType, name, methodDescriptor, operands)
        methodReturn.getOrElse(
            super.invokevirtual(pc, declaringClass, name, methodDescriptor, operands))
    }

    /**
     * Default lookup operation to check whether
     * there are stored summary results for the calling Method
     *
     * @return the optional MethodCallResult if found
     */
    private def lookup(declaringClass: ObjectType,
                       name: String,
                       methodDescriptor: MethodDescriptor,
                       operands: Operands): Option[MethodCallResult] = {

        if (!results.contains((declaringClass, name, methodDescriptor))) {
            None
        } else {
            //debugSet += ((declaringClass, name, methodDescriptor))
            val summaryInformation = results.get((declaringClass.asObjectType, name, methodDescriptor)).get
            val methodCallResult = evaluateStoredInformation(
                getPotentialExceptions(operands, methodDescriptor, summaryInformation._2),
                summaryInformation._1.asInstanceOf[Option[DomainValue]])
            methodCallResult
        }
    }

    /**
     * Returns the appropriate MethodCallResult if corresponding Summary Information have been found
     *
     * @param potExceptions the potential Exceptions that might have been thrown
     * @param potReturnValue the potential ReturnValue of the method
     *
     * @return the expected MethodCallResult
     */
    private def evaluateStoredInformation(
        potExceptions: callingDomain.ExceptionValues,
        potReturnValue: Option[DomainValue]): Option[MethodCallResult] = {
        if (potReturnValue.isEmpty && potExceptions.isEmpty) {
            None
        } else if (potReturnValue.isEmpty) {
            Some(ThrowsException(potExceptions))
        } else {
            //adapt return value:
            val intRange = potReturnValue.get.asInstanceOf[originDomain.IntegerRange]
            val returnValue = callingDomain.IntegerRange(intRange.lowerBound, intRange.upperBound)
            Some(MethodCallResult(returnValue, potExceptions))
        }
    }

    /**
     * Matches the current operands against those stored and associated with
     * exceptions and returns the appropriate exceptions upon a match.
     *
     * @param operands The operands used by the current invoke call
     * @param methodDescriptor The MethodDescriptor of the current method calling
     * @param storedExceptionValues The stored exceptions of the summary
     *
     * @return List of potentially thrown exceptions that match the operands of the calling method.
     */
    private def getPotentialExceptions(operands: Operands,
                                       methodDescriptor: MethodDescriptor,
                                       storedExceptionValues: StoredExceptionMap): List[callingDomain.ExceptionValue] = {

        /*
         * Helper methods do adapt exception values to a certain domain.
         */
        def adaptExceptions(storedExceptionValues: StoredExceptionMap): StoredExceptionMap = {
            storedExceptionValues.toList.map(listEntry ⇒
                (listEntry._1 -> listEntry._2.map(exception ⇒
                    exception.asInstanceOf[originDomain.DomainSingleOriginReferenceValue]
                        .adapt(callingDomain, exception.asInstanceOf[originDomain.DomainSingleOriginReferenceValue].origin)).toSet))
                .asInstanceOf[List[(org.opalj.ai.ValuesDomain#Locals, callingDomain.ExceptionValue)]].toMap
                .asInstanceOf[StoredExceptionMap]
        }
        /*
         * checks if the passed method operands confirm to stored operands
         */
        def correspondingOperatorsMatch(operands: originDomain.Operands, callingOperands: originDomain.Operands): Boolean = {
            callingOperands.zip(operands).forall { x ⇒
                if (x._1 == null && x._2 == null) {
                    true
                } else if (x._1 == null || x._2 == null) {
                    false
                } else if (x._1.isInstanceOf[originDomain.IntegerRange]
                    && x._2.isInstanceOf[originDomain.IntegerRange]) {
                    x._2.abstractsOver(x._1)
                } else if (x._1.isInstanceOf[originDomain.AnIntegerValue]
                    || x._2.isInstanceOf[originDomain.AnIntegerValue]) {
                    if (x._1.isInstanceOf[originDomain.IntegerRange]) {
                        val storedVal = x._1.asInstanceOf[originDomain.IntegerRange]
                        (storedVal.lowerBound == Int.MinValue && storedVal.upperBound == Int.MaxValue)
                    } else if (x._1.isInstanceOf[originDomain.AnIntegerValue] && x._2.isInstanceOf[originDomain.IntegerRange]) {
                        val callingVal = x._2.asInstanceOf[originDomain.IntegerRange]
                        (callingVal.lowerBound == Int.MinValue && callingVal.upperBound == Int.MaxValue)
                    } else if (x._1.isInstanceOf[originDomain.AnIntegerValue] && x._2.isInstanceOf[originDomain.AnIntegerValue]) {
                        true
                    } else false
                } else {
                    true //x._1.abstractsOver(x._2) - ignoring anything that is not an integer value for now.
                }
            }
        }

        // the simple case - there are no stored exceptions for the method
        if (storedExceptionValues.isEmpty) {
            List.empty
        } else {
            var exceptions: Set[callingDomain.ExceptionValue] = Set.empty
            val adaptedExceptions = adaptExceptions(storedExceptionValues)
            // check if any stored locals match those by the method call
            // and if so return the appropriate exceptions
            adaptedExceptions.foreach { exceptionMapping ⇒
                if (correspondingOperatorsMatch(
                    exceptionMapping._1.asInstanceOf[originDomain.Operands],
                    org.opalj.ai.mapOperands(operands, originDomain).toList)) {
                    exceptions = (exceptions ++ exceptionMapping._2.asInstanceOf[Set[callingDomain.ExceptionValue]])
                }
            }
            exceptions.toList
        }
    }
}

/**
 * The companion object holding the map for the summary information,
 * so that once initiated can be accessed by multiple domains
 * without the need for re-initiation.
 */
object LibraryMethodsHandling {
    val initDomain = new DefaultSummaryValuesInitDomain()
    val results = {
        // Helper method to load the summary from its name
        def summaryName2SummaryClass(str: String): ASummary = {
            val prefix = "org.opalj.ai.domain.summaries."
            val clazz = Class.forName(prefix + str)
            val obj = clazz.newInstance()
            obj.asInstanceOf[ASummary]
        }

        val summaries = org.opalj.ai.tools.FileOperations.getSummaryPropertiesFileContent
        val retrievedResults: LibraryMethodsHandling#SummaryMapType = AnyRefMap.empty
        summaries.values.foreach { summaryName ⇒
            println("Retrieving results from: "+summaryName)
            try {
                retrievedResults ++= summaryName2SummaryClass(summaryName).init(initDomain)
            } catch {
                case t: Throwable ⇒
                    println("ERROR: Summary "+summaryName+
                        " could not be loaded - most likely not included on the classpath")
                    throw t
            }
        }

        retrievedResults
    }
}