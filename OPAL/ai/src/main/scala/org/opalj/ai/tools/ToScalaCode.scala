/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.ai.tools

import org.opalj.br.Method
import org.opalj.br.MethodDescriptor
import org.opalj.br.ObjectType
import org.opalj.br.VoidType
import org.opalj.br.IntegerType
import org.opalj.br.CharType
import org.opalj.br.BooleanType
import org.opalj.br.DoubleType
import org.opalj.br.FloatType
import org.opalj.br.LongType
import org.opalj.br.ShortType
import org.opalj.br.ByteType
import org.opalj.br.FieldType
import org.opalj.br.Type
import org.opalj.br.Code
import org.opalj.br.ArrayType

/**
 * Simple type-class to parse language elements to a valid source code representation.
 *
 * @author Marius Diebel
 */
trait ToScalaCode[T] {
    def makeCode(scalaElement: T): String
}
object ToScalaCode {
    val qM = "\""
    implicit object ScalaElementIntegerRange extends ToScalaCode[RecordAnalysisResults#IntegerRangeValue] {
        def makeCode(scalaElement: RecordAnalysisResults#IntegerRangeValue): String = s"d.IntegerRange(${scalaElement.asInstanceOf[RecordAnalysisResults#IntegerRangeValue].lowerBound}, ${scalaElement.asInstanceOf[RecordAnalysisResults#IntegerRangeValue].upperBound})"
    }
    implicit object ScalaElementObjectType extends ToScalaCode[ObjectType] {
        def makeCode(scalaElement: ObjectType): String = {
            s"ObjectType(${qM + scalaElement.fqn + qM})"
        }
    }
    implicit object ScalaElementArrayType extends ToScalaCode[ArrayType] {
        def makeCode(scalaElement: ArrayType): String = {
            s"ArrayType(${ScalaElementFieldType.makeCode(scalaElement.asInstanceOf[org.opalj.br.ArrayType].elementType)})"
        }
    }
    implicit object ScalaElementMethodDescriptor extends ToScalaCode[MethodDescriptor] {
        def makeCode(scalaElement: MethodDescriptor): String = {
            scalaElement.parametersCount match {
                case 0 ⇒ s"MethodDescriptor(IndexedSeq.empty, ${ScalaElementType.makeCode(scalaElement.returnType)})"
                case 1 ⇒ s"MethodDescriptor(${ScalaElementFieldType.makeCode(scalaElement.parameterType(0))}, ${ScalaElementType.makeCode(scalaElement.returnType)})"
                case 2 ⇒ s"MethodDescriptor(IndexedSeq(${ScalaElementFieldType.makeCode(scalaElement.parameterType(0))} , ${ScalaElementFieldType.makeCode(scalaElement.parameterType(1))}), ${ScalaElementType.makeCode(scalaElement.returnType)})"
                case _ ⇒ {
                    val str = new StringBuffer
                    scalaElement.parameterTypes.foreach { x ⇒ str.append(ScalaElementFieldType.makeCode(x)+", ") }
                    s"MethodDescriptor(IndexedSeq(${str.toString().dropRight(2)}), ${ScalaElementType.makeCode(scalaElement.returnType)})"
                }
            }
        }
    }
    implicit object ScalaElementType extends ToScalaCode[Type] {
        def makeCode(scalaElement: Type): String = scalaElement match {
            case CharType    ⇒ "org.opalj.br.CharType"
            case DoubleType  ⇒ "org.opalj.br.DoubleType"
            case FloatType   ⇒ "org.opalj.br.FloatType"
            case IntegerType ⇒ "org.opalj.br.IntegerType"
            case LongType    ⇒ "org.opalj.br.LongType"
            case ShortType   ⇒ "org.opalj.br.ShortType"
            case BooleanType ⇒ "org.opalj.br.BooleanType"
            case VoidType    ⇒ "org.opalj.br.VoidType"
            case ByteType    ⇒ "org.opalj.br.ByteType"
            case _ ⇒ if (scalaElement.isObjectType)
                ScalaElementObjectType.makeCode(scalaElement.asInstanceOf[org.opalj.br.ObjectType])
            else
                ScalaElementArrayType.makeCode(scalaElement.asInstanceOf[org.opalj.br.ArrayType])
        }
    }
    implicit object ScalaElementFieldType extends ToScalaCode[FieldType] {
        def makeCode(scalaElement: FieldType): String = {
            scalaElement match {
                case CharType    ⇒ "org.opalj.br.CharType"
                case DoubleType  ⇒ "org.opalj.br.DoubleType"
                case FloatType   ⇒ "org.opalj.br.FloatType"
                case IntegerType ⇒ "org.opalj.br.IntegerType"
                case LongType    ⇒ "org.opalj.br.LongType"
                case ShortType   ⇒ "org.opalj.br.ShortType"
                case BooleanType ⇒ "org.opalj.br.BooleanType"
                case ByteType    ⇒ "org.opalj.br.ByteType"
                case _ ⇒ if (scalaElement.isObjectType)
                    ScalaElementObjectType.makeCode(scalaElement.asInstanceOf[org.opalj.br.ObjectType])
                else
                    ScalaElementArrayType.makeCode(scalaElement.asInstanceOf[org.opalj.br.ArrayType])
            }
        }
    }
    implicit object ScalaElementCode extends ToScalaCode[Option[Code]] {
        // pattern matching to the right type here
        def makeCode(scalaElement: Option[Code]): String = {
            if (scalaElement.isEmpty)
                ""
            else
                s"Code(${scalaElement.get.maxStack}, ${scalaElement.get.maxLocals}, ${scalaElement.get.instructions}, ${scalaElement.get.exceptionHandlers}, ${scalaElement.get.attributes})"
        }
    }
    implicit object ScalaElementException extends ToScalaCode[org.opalj.ai.ValuesDomain#ExceptionValue] {
        def makeCode(scalaElement: org.opalj.ai.ValuesDomain#ExceptionValue): String = {
            val origin: Int = scalaElement.asInstanceOf[RecordAnalysisResults#SingleOriginRefVal].origin
            val objectType: ObjectType = scalaElement.asInstanceOf[org.opalj.ai.domain.l1.ReferenceValues#ObjectValue].upperTypeBound.head.asInstanceOf[ObjectType]
            objectType match {
                case org.opalj.br.ObjectType.NullPointerException ⇒ s"d.NullPointerException(${origin})"
                case org.opalj.br.ObjectType.ClassCastException ⇒ s"d.ClassCastException(${origin})"
                case org.opalj.br.ObjectType.Throwable ⇒ s"d.Throwable(${origin})"
                case org.opalj.br.ObjectType.ClassNotFoundException ⇒ s"d.ClassNotFoundException(${origin})"
                case org.opalj.br.ObjectType.NegativeArraySizeException ⇒ s"d.NegativeArraySizeException(${origin})"
                case org.opalj.br.ObjectType.ArrayIndexOutOfBoundsException ⇒ s"d.ArrayIndexOutOfBoundsException(${origin})"
                case org.opalj.br.ObjectType.ArrayStoreException ⇒ s"d.ArrayStoreException(${origin})"
                case org.opalj.br.ObjectType.ArithmeticException ⇒ s"d.ArithmeticException(${origin})"
                case _ ⇒ s"d.InitializedObjectValue(${origin}, ${ScalaElementObjectType.makeCode(objectType)})"
            }
        }
    }
    implicit object ScalaElementOptionDomainValue extends ToScalaCode[Option[org.opalj.ai.ValuesDomain#DomainValue]] {
        def makeCode(scalaElement: Option[org.opalj.ai.ValuesDomain#DomainValue]): String = {
            if (scalaElement.isEmpty)
                "None"
            else {
                val casting = ".asInstanceOf[DomainValue]"
                s"Option(${ScalaElementIntegerRange.makeCode(scalaElement.get.asInstanceOf[RecordAnalysisResults#IntegerRangeValue])}${casting})"
            }
        }
    }
    implicit object ScalaElementDomainValue extends ToScalaCode[org.opalj.ai.ValuesDomain#DomainValue] {
        def makeCode(scalaElement: org.opalj.ai.ValuesDomain#DomainValue): String = {
            val intMatcher1 = """int ∈ \[[-?0-9]+,[-?0-9]+\]""".r
            val intMatcher2 = """int = [-?0-9]+""".r
            val arrayMatcher = """.+\[\],.+""".r
            val mobjectMatcher = """.+<: .+\[.+\]""".r
            val sobjectMatcher = """.+\[.+;.+\]""".r

            if (scalaElement == null) "null" else
                scalaElement.toString() match {
                    case intMatcher1()    ⇒ ScalaElementIntegerRange.makeCode(scalaElement.asInstanceOf[RecordAnalysisResults#IntegerRangeValue])
                    case intMatcher2()    ⇒ ScalaElementIntegerRange.makeCode(scalaElement.asInstanceOf[RecordAnalysisResults#IntegerRangeValue])
                    case arrayMatcher()   ⇒ ScalaElementArrayValue.makeCode(scalaElement.asInstanceOf[org.opalj.ai.domain.l1.ReferenceValues#DomainArrayValue])
                    case mobjectMatcher() ⇒ ScalaElementObjectValue.makeCode(scalaElement.asInstanceOf[org.opalj.ai.domain.l1.ReferenceValues#ObjectValue]) //mobjectMatcheractually
                    case sobjectMatcher() ⇒ ScalaElementObjectValue.makeCode(scalaElement.asInstanceOf[org.opalj.ai.domain.l1.ReferenceValues#ObjectValue]) //mobjectMatcheractually
                    case _                ⇒ "d."+scalaElement.toString
                }
        }
    }
    implicit object ScalaElementExceptionMap extends ToScalaCode[Map[org.opalj.ai.ValuesDomain#Operands, Set[org.opalj.ai.ValuesDomain#ExceptionValue]]] {
        def makeCode(scalaElement: Map[org.opalj.ai.ValuesDomain#Operands, Set[org.opalj.ai.ValuesDomain#ExceptionValue]]): String = {
            val casting = ".asInstanceOf[Map[List[org.opalj.ai.ValuesDomain#DomainValue],Set[org.opalj.ai.ValuesDomain#DomainReferenceValue]]]"
            if (scalaElement.isEmpty) "Map.empty"+casting
            else {
                val str = new StringBuffer
                scalaElement.foreach { e ⇒
                    str.append(ScalaElementOperands.makeCode(e._1)+"->"+ScalaElementExceptionValuesSet.makeCode(e._2)+", ")
                }
                s"Map(${str.toString().dropRight(2)})${casting}"
            }
        }
    }
    //    implicit object ScalaElementLocals extends ToScalaCode[org.opalj.ai.ValuesDomain#Locals] {
    //        def makeCode(scalaElement: org.opalj.ai.ValuesDomain#Locals): String = {
    //            val localsDomainValues: Seq[String] = scalaElement.toSeq.asInstanceOf[Seq[org.opalj.ai.ValuesDomain#DomainValue]].map { x ⇒ ScalaElementDomainValue.makeCode(x) }
    //            val str = new StringBuffer
    //            var i = 0
    //            localsDomainValues.foreach { x ⇒
    //                str.append(s"locals.set(${i}, ${x})"+"\n")
    //                i = i + 1
    //            }
    //            val maxLocals = scalaElement.size
    //            s"{ val locals = org.opalj.collection.mutable.Locals[callingDomain.DomainValue](${maxLocals}) \n"+str.toString()+" locals }"
    //        }
    //    }
    implicit object ScalaElementOperands extends ToScalaCode[org.opalj.ai.ValuesDomain#Operands] {
        def makeCode(scalaElement: org.opalj.ai.ValuesDomain#Operands): String = {
            val casting = ".asInstanceOf[List[DomainValue]]"
            if (scalaElement.isEmpty) {
                "List.empty"+casting
            } else {
                val localsDomainValues: Seq[String] = scalaElement.toSeq.asInstanceOf[Seq[org.opalj.ai.ValuesDomain#DomainValue]].map { x ⇒ ScalaElementDomainValue.makeCode(x) }
                val str = new StringBuffer
                str.append("List(")
                localsDomainValues.foreach { x ⇒
                    str.append(x+", ")
                }
                s"${str.toString.dropRight(2)})${casting}"
            }
        }
    }
    implicit object ScalaElementExceptionValuesSet extends ToScalaCode[Set[org.opalj.ai.ValuesDomain#ExceptionValue]] {
        def makeCode(scalaElement: Set[org.opalj.ai.ValuesDomain#ExceptionValue]): String = {
            val casting = ".asInstanceOf[Set[ExceptionValue]]"
            if (scalaElement.isEmpty) "Set.empty"+casting // should never occur
            else {
                val str = new StringBuffer
                scalaElement.foreach { x ⇒ str.append(ScalaElementException.makeCode(x)+", ") }
                s"Set(${str.toString().dropRight(2)})${casting}"
            }
        }
    }
    implicit object ScalaElementObjectValue extends ToScalaCode[org.opalj.ai.domain.l1.ReferenceValues#ObjectValue] {
        def makeCode(scalaElement: org.opalj.ai.domain.l1.ReferenceValues#ObjectValue): String = {
            if (scalaElement.upperTypeBound.size == 1) {
                val upperType = ScalaElementObjectType.makeCode(scalaElement.upperTypeBound.head.asInstanceOf[org.opalj.br.ObjectType])
                s"d.ObjectValue(${scalaElement.origin}, ${scalaElement.isNull}, ${scalaElement.isPrecise}, ${upperType}, ${scalaElement.t})"
            } else {
                val strBuffer = new StringBuffer
                scalaElement.upperTypeBound.foreach { x ⇒ strBuffer.append(ScalaElementObjectType.makeCode(x.asInstanceOf[org.opalj.br.ObjectType])+", ") }
                val uidSet = s"org.opalj.collection.immutable.UIDSet(${strBuffer.toString().dropRight(2)})"
                s"d.ObjectValue(${scalaElement.origin}, ${scalaElement.isNull}, ${uidSet}, ${scalaElement.t})"
            }
        }
    }
    implicit object ScalaElementArrayValue extends ToScalaCode[org.opalj.ai.domain.l1.ReferenceValues#DomainArrayValue] {
        def makeCode(scalaElement: org.opalj.ai.domain.l1.ReferenceValues#DomainArrayValue): String = {
            val arrType = scalaElement.upperTypeBound
            val upperTypeBound = ScalaElementArrayType.makeCode(arrType.head.asInstanceOf[org.opalj.br.ArrayType])
            s"d.ArrayValue(${scalaElement.origin}, ${scalaElement.isNull}, ${scalaElement.isPrecise}, ${upperTypeBound}, ${scalaElement.t})"
        }
    }
    implicit object ScalaElementOperandsExceptionEntry extends ToScalaCode[(org.opalj.ai.ValuesDomain#Operands, Set[org.opalj.ai.ValuesDomain#ExceptionValue])] {
        def makeCode(scalaElement: (org.opalj.ai.ValuesDomain#Operands, Set[org.opalj.ai.ValuesDomain#ExceptionValue])): String = {
            s"(${ScalaElementOperands.makeCode(scalaElement._1)} -> ${ScalaElementExceptionValuesSet.makeCode(scalaElement._2)})"
        }
    }
}